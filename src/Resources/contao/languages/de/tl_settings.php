<?php

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_settings']['ntonl_news_groups'] = array('aus welchen Newsarchiven', 'Geben Sie die Newsarchive an die bei der Generierung berücksichtigt werden sollen.');
$GLOBALS['TL_LANG']['tl_settings']['ntonl_nl_channel'] = array('zu welchem Newsletter-Verteiler', 'Geben Sie den Newsletter-Verteiler an, zu dem der erstellte Newsletter zu geordnet wird.');
$GLOBALS['TL_LANG']['tl_settings']['ntonl_senderName']     = array('Absendername', 'Hier können Sie den Namen des Absenders eingeben.');
$GLOBALS['TL_LANG']['tl_settings']['ntonl_sender']         = array('Absenderadresse', 'Hier können Sie eine individuelle Absenderadresse eingeben.');

/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_settings']['ntonl_legend']      = 'Newsletter aus News Einstellungen';
